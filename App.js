import React from 'react';
import {Text} from 'react-native';
import OneSignal from 'react-native-onesignal';

class App extends React.Component {
    componentDidMount() {
        OneSignal.init('81a9dced-2283-4acf-985a-79c6d552119b');
        OneSignal.addEventListener('received', (data) => {
            console.log(data);
        });
        OneSignal.inFocusDisplaying(2);
    }

    componentWillUnmount() {
        OneSignal.removeEventListener('received');
    }

    render() {
        return (
            <Text>testing</Text>
        );
    }
}

export default (App);
